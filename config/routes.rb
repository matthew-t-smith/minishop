Rails.application.routes.draw do
  get '/', to: 'welcome#index'
  get '/settings', to: 'welcome#settings'

  get '/customer_login', to: 'sessions#customer_login_page'
  post '/customer_login', to: 'sessions#customer_login'
  get '/admin_login', to: 'sessions#admin_login_page'
  post '/admin_login', to: 'sessions#admin_login'
  delete '/logout', to: 'sessions#destroy'

  resources :customers, only: %i(index new create) do
    resources :readings, only: %i(index new create)
  end

  resources :admins, only: %i(index)

  namespace :admin do
    resources :customers, only: %i(index show update) do
      post '/invoice', to: "customers#mail"
      resources :readings, only: %i(index new create)
    end
    resources :addresses, only: %i(index new create)
    resources :prices, only: %i(new create)
  end

  root 'welcome#index'
end
