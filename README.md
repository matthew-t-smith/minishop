# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.5.1p57

* System dependencies

* Configuration - MySQL >> MariaDB
  - `mysql.server start` - Starts database up
  - `rails db:environment:set RAILS_ENV=development` - Choice of development, test, production
  - `rails server` - Starts web server to deliver

* Database creation
  - `rails db:create` - Creates the DB
  - `rails db:migrate` - Migrates changes to DB
  - `rails db:seed` - Seeds the DB

* Database initialization
  - `mysql.server start` - Starts database up
  - `mysql -u root` - Logs into DB

* How to run the test suite
  - `bundle exec rspec [directory]` - Runs Specs at whichever directory level you define

* Helpful things
  - http://localhost:3000/rails/info/routes - Shows routes (same as `rails routes`, but with full path names)

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
