require 'rails_helper'

RSpec.describe 'Welcome', type: :request do

  describe 'GET /' do
    it 'successfully goes to the root index page' do
      get '/'

      expect(response).to have_http_status(200)
      expect(response).to render_template(:index)
    end
  end

  describe 'GET /settings' do
    it 'successfully loads the settings page' do
      get '/settings'

      expect(response).to have_http_status(200)
      expect(response).to render_template(:settings)
    end
  end
end