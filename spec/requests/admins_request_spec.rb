require 'rails_helper'

RSpec.describe 'Admins', type: :request do
  fixtures :admins

  describe 'GET /admins' do
    context 'when an admin is logged in' do
      it 'loads the admin page' do
        post '/admin_login', params: { email: admins(:admin).email }
        get '/admins'

        expect(response).to have_http_status(200)
        expect(response).to render_template(:index)
      end
    end

    context 'when an admin is not logged in' do
      it 'redirects to the root page' do
        get '/admins'

        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_url

        follow_redirect!

        expect(response).to render_template(:index)
      end
    end
  end
end