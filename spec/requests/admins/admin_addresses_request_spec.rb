require 'rails_helper'

RSpec.describe 'Addresses', type: :request do

  describe 'GET /admin/addresses'

  describe 'POST /admin/addresses' do
    context 'when address successfully saves' do
      xit 'loads the address page' do
        post '/admin/addresses', params: { address: {
          street_number: 700,
          street: 'Avenue',
          unit: '',
          suburb: 'Suburb',
          town: 'Town',
          zip_code: '1001',
          icp: '1234543210ABCDE'
        } }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to addresses_url

        follow_redirect!

        expect(response).to render_template(:index)
      end
    end

    context 'when address unsuccessfully saves' do
      xit 'redirects to the new address page' do
      end
    end
  end

  describe 'GET /admin/addresses/new' do
    xit 'loads the new address page' do
      get '/admin/addresses/new'

      expect(response).to have_http_status(200)
      expect(response).to render_template(:new)
    end
  end
end