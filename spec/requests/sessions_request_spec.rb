require 'rails_helper'

RSpec.describe 'Sessions', type: :request do
  fixtures :customers, :admins

  describe 'GET /customer_login' do
    context 'when customer is logged in' do
      it 'redirects to the customer page' do
        post '/customer_login', params: { 
          email: customers(:harry_potter).email 
        }
        get '/customer_login'

        expect(response).to have_http_status(302)
        expect(response).to redirect_to customers_url

        follow_redirect!
        
        expect(response).to render_template(:index)
      end
    end
    
    context 'when customer is not logged in' do
      it 'successfully loads the customer login page' do
        get '/customer_login'

        expect(response).to have_http_status(200)
        expect(response).to render_template(:customer_login_page)
      end
    end
  end

  describe 'POST /customer_login' do
    context 'when logging in with valid credentials' do
      it 'redirects to the customer page' do
        post '/customer_login', params: { 
          email: customers(:harry_potter).email 
        }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to customers_url

        follow_redirect!
        
        expect(response).to render_template(:index)
      end
    end

    context 'when logging in with invalid credentials' do
      it 'gets the customer login page again' do
        post '/customer_login', params: { email: 'invalid' }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to customer_login_url

        follow_redirect!
        
        expect(response).to render_template(:customer_login_page)
      end
    end
  end

  describe 'GET /admin_login' do
    context 'when admin is logged in' do
      it 'redirects to the admin page' do
        post '/admin_login', params: { email: admins(:admin).email }
        get '/admin_login'

        expect(response).to have_http_status(302)
        expect(response).to redirect_to admins_url

        follow_redirect!
        
        expect(response).to render_template(:index)
      end
    end
    
    context 'when admin is not logged in' do
      it 'successfully loads the admin login page' do
        get '/admin_login'

        expect(response).to have_http_status(200)
        expect(response).to render_template(:admin_login_page)
      end
    end
  end

  describe 'POST /admin_login' do
    context 'when logging in with valid credentials' do
      it 'redirects to the admin page' do
        post '/admin_login', params: { email: admins(:admin).email }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to admins_url

        follow_redirect!
        
        expect(response).to render_template(:index)
      end
    end

    context 'when logging in with invalid credentials' do
      it 'gets the admin login page again' do
        post '/admin_login', params: { email: 'invalid' }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to admin_login_url

        follow_redirect!
        
        expect(response).to render_template(:admin_login_page)
      end
    end
  end

  describe 'DELETE /logout' do
    it 'redirects to the root page' do
      delete '/logout'

      expect(response).to have_http_status(302)
      expect(response).to redirect_to root_url

      follow_redirect!

      expect(response).to render_template(:index)
    end
  end
end
