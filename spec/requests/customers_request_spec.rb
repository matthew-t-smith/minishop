require 'rails_helper'

RSpec.describe 'Customers', type: :request do
  fixtures :customers

  describe 'GET /customers' do
    context 'when customer is logged in' do
      it 'loads the customer page' do
        post '/customer_login', params: {
          email: customers(:harry_potter).email
        }
        get '/customers'

        expect(response).to have_http_status(200)
        expect(response).to render_template(:index)
      end
    end

    context 'when customer is not logged in' do
      it 'redirects to the root page' do
        get '/customers'

        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_url

        follow_redirect!

        expect(response).to render_template(:index)
      end
    end
  end

  describe 'POST /customers' do
    context 'when customer successfully signs up' do
      it 'redirects to the customer page' do
        post '/customers', params: { address: {
          street_number: 700,
          street: 'Avenue',
          unit: '',
          suburb: 'Suburb',
          town: 'Town',
          zip_code: '1001',
          icp: '1234567890ABCDE'
        }, customer: {
          first_name: 'John',
          last_name: 'Doe',
          email: 'john.doe@spoof.com'
        } }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to customers_url

        follow_redirect!

        expect(response).to render_template(:index)
      end
    end

    context 'when customer unsuccessfully signs up' do
      it 'redirects to the new customer page' do
        post '/customers', params: { address: {
          street_number: nil,
          street: nil,
          unit: nil,
          suburb: nil,
          town: nil,
          zip_code: nil,
          icp: nil
        }, customer: {
          first_name: nil,
          last_name: nil,
          email: nil
        } }

        expect(response).to have_http_status(302)
        expect(response).to redirect_to new_customer_url

        follow_redirect!

        expect(response).to render_template(:new)
      end
    end
  end

  describe 'GET /customers/new' do
    it 'loads the new customer page' do
      get '/customers/new'

      expect(response).to have_http_status(200)
      expect(response).to render_template(:new)
    end
  end
end
