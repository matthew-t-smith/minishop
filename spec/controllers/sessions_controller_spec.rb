require 'rails_helper'
include SessionsHelper

RSpec.describe SessionsController, type: :controller do
  fixtures :customers, :admins

  describe 'GET #customer_login_page' do
    context 'when customer is logged in' do
      it 'redirects to customers_url' do
        log_customer_in(customers(:harry_potter))
        get :customer_login_page

        expect(response).to redirect_to customers_url
      end
    end

    context 'when customer is not logged in' do
      it 'renders the customer login page' do
        get :customer_login_page

        expect(response).to render_template(:customer_login_page)
      end
    end
  end

  describe 'GET #admin_login_page' do
    context 'when admin is logged in' do
      it 'redirects to admins_url' do
        log_admin_in(admins(:admin))
        get :admin_login_page

        expect(response).to redirect_to admins_url
      end
    end

    context 'when admin is not logged in' do
      it 'renders the admin login page' do
        get :admin_login_page

        expect(response).to render_template(:admin_login_page)
      end
    end
  end

  describe 'POST #customer_login' do
    context 'when logging in a valid customer' do
      it 'assigns the correct instance variable' do
        post :customer_login, params: { email: customers(:harry_potter).email }

        expect(assigns(:customer)).to eq customers(:harry_potter)
      end

      it 'logs the customer in' do
        post :customer_login, params: { email: customers(:harry_potter).email }

        expect(customer_logged_in?).to be true
      end

      it 'goes to the customer page' do
        post :customer_login, params: { email: customers(:harry_potter).email }

        expect(request).to redirect_to customers_url
      end
    end

    context 'when logging in an invalid customer' do
      it 'creates an error message' do
        post :customer_login, params: { email: nil }

        expect(flash[:error]).to be_present
        expect(flash[:error]).to include 'Not a valid customer...'
      end

      it 'redirects to the login page' do
        post :customer_login, params: { email: nil }

        expect(request).to redirect_to customer_login_url
      end
    end
  end

  describe 'POST #admin_login' do
    context 'when logging in a valid admin' do
      it 'assigns the correct instance variable' do
        post :admin_login, params: { email: admins(:admin).email }

        expect(assigns(:admin)).to eq admins(:admin)
      end

      it 'logs the admin in' do
        post :admin_login, params: { email: admins(:admin).email }

        expect(admin_logged_in?).to be true
      end

      it 'goes to the admin page' do
        post :admin_login, params: { email: admins(:admin).email }

        expect(request).to redirect_to admins_url
      end
    end

    context 'when logging in an invalid admin' do
      it 'creates an error message' do
        post :admin_login, params: { email: nil }

        expect(flash[:error]).to be_present
        expect(flash[:error]).to include 'Not a valid admin...'
      end

      it 'redirects to the login page' do
        post :admin_login, params: { email: nil }

        expect(request).to redirect_to admin_login_url
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when customer is logged in' do
      it 'logs the customer out' do
        log_customer_in(customers(:harry_potter))
        delete :destroy

        expect(customer_logged_in?).to be false
      end

      it 'redirects to the root page' do
        log_customer_in(customers(:harry_potter))
        delete :destroy

        expect(response).to redirect_to root_url
      end
    end

    context 'when admin is logged in' do
      it 'logs the admin out' do
        log_admin_in(admins(:admin))
        delete :destroy

        expect(admin_logged_in?).to be false
      end

      it 'redirects to the root page' do
        log_admin_in(admins(:admin))
        delete :destroy

        expect(response).to redirect_to root_url
      end
    end

    context 'when no one is logged in' do
      it 'redirects to the root page' do
        delete :destroy

        expect(response).to redirect_to root_url
      end
    end
  end
end