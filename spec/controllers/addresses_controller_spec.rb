require 'rails_helper'

RSpec.describe AddressesController, type: :controller do
  fixtures :addresses

  describe 'GET #new' do
    it 'creates a new Address object (empty)' do
      get :new

      expect(assigns(:address)).to be_an_instance_of Address
    end
  end

  describe 'POST #create' do
    context 'when saving can succeed' do
      it 'creates a new Address instance based on passed params' do
        post :create, params: { address: {
          street_number: addresses(:potter_address).street_number,
          street: addresses(:potter_address).street,
          unit: addresses(:potter_address).unit,
          suburb: addresses(:potter_address).suburb,
          town: addresses(:potter_address).town,
          zip_code: addresses(:potter_address).zip_code,
          country: addresses(:potter_address).country,
          icp: addresses(:potter_address).icp
        } }

        expect(assigns(:address)).to be_an_instance_of Address
        expect(assigns(:address).unverified?).to be true
        expect(flash[:error]).not_to be_present
      end
    end

    context 'when saving hits an error' do
      it 'builds an error message' do
        post :create, params: { address: {
          street_number: nil,
          street: nil,
          unit: nil,
          suburb: nil,
          town: nil,
          zip_code: nil,
          icp: nil
        } }

        expect(flash[:error]).to be_present
        expect(flash[:error]).to include('Address Errors:')
      end
    end
  end
end
