require 'rails_helper'
include SessionsHelper

RSpec.describe CustomersController, type: :controller do
  fixtures :customers, :addresses

  describe 'GET #index' do
    context 'when customer is logged in' do
      it 'evaluates the correct instance variable' do
        log_customer_in customers(:winnie_the_pooh)
        get :index

        expect(assigns(:customer)).to eq customers(:winnie_the_pooh)
      end
    end

    context 'when customer is not logged in' do
      it 'creates an error message' do
        get :index

        expect(flash[:error]).to be_present
        expect(flash[:error]).to include 'Not logged in.'
      end
    end
  end

  describe 'GET #new' do
    it 'creates new Address and Customer objects (empty)' do
      get :new

      expect(assigns(:address)).to be_an_instance_of Address
      expect(assigns(:customer)).to be_an_instance_of Customer
    end
  end

  describe 'POST #create' do
    context 'when saving can succeed' do
      it 'creates new Address and Customer instances based on passed params' do
        post :create, params: { address: {
          street_number: 101,
          street: 'Street',
          unit: '4b',
          suburb: 'Suburb',
          town: 'Town',
          zip_code: 4004,
          icp: '001122334455667'
        }, customer: {
          first_name: 'First',
          last_name: 'Last',
          email: 'email@spoof.com'
        } }

        expect(assigns(:address)).to be_an_instance_of Address
        expect(assigns(:address).unverified?).to be true
        expect(assigns(:customer)).to be_an_instance_of Customer
        expect(flash[:error]).not_to be_present
      end

      it 'logs the customer in' do
        post :create, params: { address: {
          street_number: 700,
          street: 'Avenue',
          unit: '',
          suburb: 'Suburb',
          town: 'Town',
          zip_code: 1001,
          icp: '0123456789ABCD'
        }, customer: {
          first_name: 'John',
          last_name: 'Doe',
          email: 'john.doe@spoof.com'
        } }

        expect(customer_logged_in?).to be true
        expect(current_customer.first_name).to eq 'John'
      end
    end

    context 'when saving hits an error' do
      it 'builds an error message' do
        post :create, params: { address: {
          street_number: nil,
          street: nil,
          unit: nil,
          suburb: nil,
          town: nil,
          zip_code: nil,
          icp: nil
        }, customer: {
          first_name: nil,
          last_name: nil,
          email: nil
        } }
        expect(flash[:error]).to be_present
        expect(flash[:error]).to include('Address Errors:')
      end
    end
  end
end