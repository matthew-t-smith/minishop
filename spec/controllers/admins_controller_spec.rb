require 'rails_helper'
include SessionsHelper

RSpec.describe AdminsController, type: :controller do
  fixtures :admins

  describe 'GET #index' do
    context 'when admin is logged in' do
      it 'evaluates the correct instance variable' do
        log_admin_in admins(:admin)
        get :index

        expect(assigns(:admin)).to eq admins(:admin)
      end
    end

    context 'when admin is not logged in' do
      it 'creates an error message' do
        get :index

        expect(flash[:error]).to be_present
        expect(flash[:error]).to include 'Not logged in as an admin.'
      end
    end
  end
end