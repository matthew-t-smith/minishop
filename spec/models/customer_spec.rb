require 'rails_helper'

RSpec.describe Customer, type: :model do
  fixtures :customers, :addresses

  describe '#create' do
    context 'when given nothing' do
      it 'is invalid' do
        expect(Customer.create).to be_invalid
      end
    end

    context 'when given all valid input' do
      it 'is valid' do
        valid_customer = Customer.create(
          first_name: 'John',
          last_name: 'Doe',
          email: 'john.doe@spoof.com',
          address: addresses(:sanders_address)
        )

        expect(valid_customer).to be_valid
      end
    end

    context 'when given a pre-existing email' do
      it 'is invalid' do
        double_customer = Customer.create(
          first_name: 'First',
          last_name: 'Last',
          email: customers(:harry_potter).email,
          address: addresses(:sanders_address)
        )

        expect(double_customer).to be_invalid
      end
    end
  end
end
