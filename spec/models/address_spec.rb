require 'rails_helper'

RSpec.describe Address, type: :model do
  fixtures :addresses

  describe '#create' do
    context 'when given nothing' do
      it 'is invalid' do
        expect(Address.create).to be_invalid
      end
    end

    context 'when given all valid properties' do
      it 'is valid' do
        valid_address = Address.create(
          street_number: 100,
          street: 'Street',
          unit: '7A',
          suburb: 'Suburb',
          town: 'Town',
          zip_code: 2951,
          icp: '1234567890ABCDE'
        )
        expect(addresses(:sanders_address)).to be_valid
      end
    end

    context 'when given the bare-minimum' do
      let(:minimum_address) {
        Address.create(street: 'Street', suburb: 'Suburb', town: 'Town', zip: 1235, icp: '1234567890ABCDE')
      }
      it 'is still valid' do
        expect(minimum_address).to be_valid
      end

      it 'defaults the country and status' do
        expect(minimum_address.country).to eq('NZ')
        expect(minimum_address.status).to eq('unverified')
      end
    end
  end
end