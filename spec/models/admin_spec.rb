require 'rails_helper'

RSpec.describe Admin, type: :model do
  fixtures :admins

  describe '#create' do
    context 'when given nothing' do
      it 'is invalid' do
        expect(Admin.create).to be_invalid
      end
    end

    context 'when given all valid input' do
      it 'is valid' do
        valid_admin = Admin.create(
          first_name: 'John',
          last_name: 'Doe',
          email: 'john.doe@admin.com'
        )

        expect(valid_admin).to be_valid
      end
    end

    context 'when given a pre-existing email' do
      it 'is invalid' do
        double_admin = Admin.create(
          first_name: 'First',
          last_name: 'Last',
          email: admins(:admin).email
        )

        expect(double_admin).to be_invalid
      end
    end
  end
end
