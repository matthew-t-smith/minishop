class AdminsController < ApplicationController
  before_action :require_login

  def index
    @admin ||= current_admin
    @price = Price.pluck(:price).last
  end

  private

  def require_login
    unless admin_logged_in?
      flash[:error] = 'Not logged in as an admin.'
      redirect_to root_url
    end
  end
end
