class SessionsController < ApplicationController
  def customer_login_page
    redirect_to customers_url if session[:customer]
  end

  def admin_login_page
    redirect_to admins_url if session[:admin]
  end

  def customer_login
    @customer = Customer.find_by(email: params[:email].downcase)
    if @customer.present?
      log_customer_in @customer
      redirect_to customers_url
    else
      flash[:error] = 'Not a valid customer...'
      redirect_to '/customer_login'
    end
  end

  def admin_login
    @admin = Admin.find_by(email: params[:email].downcase)
    if @admin.present?
      log_admin_in @admin
      redirect_to admins_url
    else
      flash[:error] = 'Not a valid admin...'
      redirect_to '/admin_login'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
