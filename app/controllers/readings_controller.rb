class ReadingsController < ApplicationController
  before_action :require_login, :find_customer
  before_action :find_all_readings, only: %i(index new)

  def index; end

  def new
    @newest_read = Reading.where(customer: @customer).last.reading || 0
    @reading = Reading.new
  end

  def create
    @reading = Reading.new(
      reading: reading_params[:reading].to_d,
      read_by: @customer,
      customer: @customer
    )

    flash[:error] = BuildErrorMessage.new(@reading).call unless @reading.save
    redirect_to customer_readings_url
  end

  private

  def require_login
    unless customer_logged_in?
      flash[:error] = 'Not logged in.'
      redirect_to root_url
    end
  end

  def reading_params
    params.require(:reading).permit(:reading, :customer)
  end

  def find_customer
    @customer = Customer.find(params[:customer_id])
  end

  def find_all_readings
    @readings = Reading.where(customer: @customer)
  end
end
