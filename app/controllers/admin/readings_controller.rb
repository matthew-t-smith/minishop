class Admin::ReadingsController < AdminsController
  before_action :find_customer
  before_action :find_all_readings, only: %i(index new)

  def index; end

  def new
    @newest_read = Reading.where(customer: @customer).last.reading || 0
    @reading = Reading.new
  end

  def create
    @admin ||= current_admin
    @reading = Reading.new(
      reading: reading_params[:reading].to_d,
      read_by: @admin,
      customer: @customer
    )

    flash[:error] = BuildErrorMessage.new(@reading).call unless @reading.save
    redirect_to admin_customer_readings_url
  end

  private

  def reading_params
    params.require(:reading).permit(:reading, :admin, :customer)
  end

  def find_customer
    @customer = Customer.find(params[:customer_id])
  end

  def find_all_readings
    @readings = Reading.where(customer: @customer)
  end
end
