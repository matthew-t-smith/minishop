class Admin::CustomersController < AdminsController
  def index
    @admin ||= current_admin
    @customers = Customer.all
  end

  def show
    @customer = Customer.find(params[:id])
    @address = @customer.address
  end

  def update
    begin
      @customer = UpdateCustomer.new(params[:id], customer_params).call
      @address = UpdateAddress.new(@customer, address_params).call
      Address.transaction do
        @address.save!
        @customer.save!
      end
    rescue ActiveRecord::RecordInvalid
      if @address.errors.any?
        flash[:error] = BuildErrorMessage.new(@address).call
      elsif @customer.errors.any?
        flash[:error] = BuildErrorMessage.new(@customer).call
      end
    end
    redirect_to admin_customers_url
  end

  def mail
    @customer = Customer.find(params[:customer_id])
    @price = Price.last.price
    @used_energy = calculate_usage(@customer)

    InvoiceMailer.with(customer: @customer, price: @price, used_energy: @used_energy).monthly_invoice.deliver_now

    redirect_to admin_customers_url
  end

  private

  def customer_params
    params.require(:customer).permit(%i[first_name last_name email])
  end

  def address_params
    params.require(:address).permit(%i[street street_number unit suburb town zip_code icp meter_decimal_places status])
  end

  def calculate_usage(customer)
    readings = Reading.where(customer: customer).last(2)
    if readings.size == 2
      readings.last.reading - readings.first.reading
    else
      readings.first.reading
    end
  end
end
