class Admin::AddressesController < AdminsController
  def index
    @admin ||= current_admin
    @addresses = Address.all
  end

  def new
    @address = Address.new
  end

  def create
    @address = BuildAddress.new(address_params).call

    if @address.save
      session[:address] = @address.id
      redirect_to admin_addresses_url
    else
      flash[:error] = BuildErrorMessage.new(@address).call
      redirect_to new_admin_addresses_url
    end
  end

  private

  def address_params
    params.require(:address).permit(:street, :street_number, :unit, :suburb, :town, :zip_code, :country, :icp)
  end
end
