class Admin::PricesController < AdminsController
  def new
    @price = Price.new
  end

  def create
    @price = Price.new(price_params)

    flash[:error] = BuildErrorMessage.new(@price).call unless @price.save
    redirect_to admins_url
  end

  private

  def price_params
    params.require(:price).permit(:price)
  end
end
