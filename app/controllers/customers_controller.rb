class CustomersController < ApplicationController
  before_action :require_login, only: :index

  def index
    @customer ||= current_customer
  end

  def new
    @address = Address.new
    @customer = Customer.new
  end

  def create
    begin
      @address = BuildAddress.new(address_params).call
      @customer = @address.build_customer(customer_params)
      Address.transaction do
        @address.save!
        @customer.save!
        log_customer_in @customer
        redirect_to customers_url and return
      end
    rescue ActiveRecord::RecordInvalid
      if @address.errors.any?
        flash[:error] = BuildErrorMessage.new(@address).call
      elsif @customer.errors.any?
        flash[:error] = BuildErrorMessage.new(@customer).call
      end
    end
    redirect_to new_customer_path
  end

  private

  def require_login
    unless customer_logged_in?
      flash[:error] = 'Not logged in.'
      redirect_to root_url
    end
  end

  def customer_params
    params.require(:customer).permit(%i[first_name last_name email address_id])
  end

  def address_params
    params.require(:address).permit(%i[street street_number unit suburb town zip_code country icp])
  end
end
