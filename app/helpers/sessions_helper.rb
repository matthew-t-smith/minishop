module SessionsHelper
  def log_admin_in(admin)
    session[:admin] = admin.id
  end

  def log_customer_in(customer)
    session[:customer] = customer.id
  end

  def current_admin
    @current_admin ||= Admin.find(session[:admin]) if session[:admin]
  end

  def current_customer
    @current_customer ||= Customer.find(session[:customer]) if session[:customer]
  end

  def customer_logged_in?
    !current_customer.nil?
  end

  def admin_logged_in?
    !current_admin.nil?
  end

  def log_out
    session.delete(:customer)
    session.delete(:admin)
    @current_user = nil
    @current_admin = nil
  end
end
