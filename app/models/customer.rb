class Customer < ApplicationRecord
  belongs_to :address
  has_many :readings
  has_many :readings, as: :read_by
  before_save :email_downcase

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, uniqueness: true, length: { maximum: 50 }, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/ }
  validates :address, presence: true

  private

  def email_downcase
    email.downcase!
  end
end
