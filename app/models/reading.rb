class Reading < ApplicationRecord
  belongs_to :customer
  belongs_to :read_by, polymorphic: true

  validates :reading, presence: true, numericality: { greater_than_or_equal_to: :previous_reading }

  private

  def previous_reading
    previous = Reading.where(customer_id: customer_id).last
    previous.nil? ? 0 : previous.reading
  end
end
