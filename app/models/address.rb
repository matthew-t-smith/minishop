class Address < ApplicationRecord
  has_one :customer
  has_many :readings, through: :customer
  enum status: %i(unverified verified connected)

  validates :street, presence: true, length: { maximum: 30 }
  validates :street_number, length: { maximum: 25 }
  validates :unit, length: { maximum: 20 }
  validates :suburb, presence: true, length: { maximum: 30 }
  validates :town, presence: true, length: { maximum: 30 }
  validates :zip_code, length: { is: 4 }
  validates :country, presence: true
  validates :icp, presence: true, uniqueness: true, length: { is: 15 }
  validates :meter_decimal_places, presence: true, length: { in: 0..4 }
end
