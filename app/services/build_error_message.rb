class BuildErrorMessage
  attr_reader :object

  def initialize(object)
    @object = object
  end

  def call
    build_message
  end

  private

  def build_message
    message = "#{object.class} Errors: "
    object.errors.full_messages.each do |msg|
      message << "\n#{msg}."
    end
    message
  end
end