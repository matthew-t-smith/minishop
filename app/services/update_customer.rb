class UpdateCustomer
  attr_reader :customer, :first_name, :last_name, :email

  def initialize(id, customer_params)
    @customer = Customer.find(id)
    @first_name = customer_params[:first_name]
    @last_name = customer_params[:last_name]
    @email = customer_params[:email]
  end

  def call
    update_customer
  end

  private

  def update_customer
    customer.first_name = first_name
    customer.last_name = last_name
    customer.email = email
    customer
  end
end
