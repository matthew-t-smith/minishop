class BuildAddress
  attr_reader :street_number, :street, :unit, :suburb, :town, :zip_code, :icp

  def initialize(address_params)
    @street_number = address_params[:street_number]
    @street = address_params[:street]
    @unit = address_params[:unit]
    @suburb = address_params[:suburb]
    @town = address_params[:town]
    @zip_code = address_params[:zip_code]
    @icp = address_params[:icp]
  end

  def call
    build_address
  end

  private

  def build_address
    Address.new(
      street_number: street_number,
      street: street,
      unit: unit,
      suburb: suburb,
      town: town,
      zip_code: zip_code,
      icp: icp
    )
  end
end
