class UpdateAddress
  attr_reader :address, :street_number, :street, :unit, :suburb, :town, :zip_code, :icp, :decimals, :status

  def initialize(customer, address_params)
    @address = customer.address
    @street_number = address_params[:street_number]
    @street = address_params[:street]
    @unit = address_params[:unit]
    @suburb = address_params[:suburb]
    @town = address_params[:town]
    @zip_code = address_params[:zip_code]
    @icp = address_params[:icp]
    @decimals = address_params[:meter_decimal_places]
    @status = address_params[:status]
  end

  def call
    update_address
  end

  private

  def update_address
    address.street_number = street_number
    address.street = street
    address.unit = unit
    address.suburb = suburb
    address.town = town
    address.zip_code = zip_code
    address.icp = icp
    address.meter_decimal_places = decimals
    address.status = status
    address
  end
end
