class InvoiceMailer < ApplicationMailer
  def monthly_invoice
    @customer = params[:customer]
    @price = params[:price]
    @used_energy = params[:used_energy]
    @bill = (@price * @used_energy).round(2)
    mail(to: @customer.email, subject: 'Monthly Invoice for Power')
  end
end
