class ApplicationMailer < ActionMailer::Base
  default from: 'admin@minishop.com'
  layout 'mailer'
end
