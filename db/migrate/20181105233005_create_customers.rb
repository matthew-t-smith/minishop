class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false, unique: true, limit: 50

      t.belongs_to :address, index: true, null: false

      t.timestamps
    end
  end
end
