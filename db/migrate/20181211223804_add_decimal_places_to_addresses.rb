class AddDecimalPlacesToAddresses < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :meter_decimal_places, :integer, null: false, default: 0
  end
end
