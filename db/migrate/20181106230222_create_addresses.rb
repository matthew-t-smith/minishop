class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street, null: false, limit: 30
      t.integer :street_number
      t.string :unit, limit: 20
      t.string :suburb, null: false, limit: 30
      t.string :town, null: false, limit: 30
      t.integer :zip_code
      t.string :country, default: 'NZ'
      t.integer :status, null: false, default: 0

      t.timestamps
    end
  end
end
