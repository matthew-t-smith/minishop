class CreateReadings < ActiveRecord::Migration[5.2]
  def change
    create_table :readings do |t|
      t.decimal :reading, precision: 16, scale: 4, null: false, unsigned: true
      t.string :read_by_type, null: false
      t.integer :read_by_id, null: false

      t.belongs_to :customer, index: true, null: false
      t.timestamps
    end

    add_index :readings, %i(read_by_type read_by_id)
  end
end
