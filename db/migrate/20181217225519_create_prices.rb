class CreatePrices < ActiveRecord::Migration[5.2]
  def change
    create_table :prices do |t|
      t.decimal :price, precision: 16, scale: 2, null: false, unsigned: true

      t.timestamps
    end
  end
end
