# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Address.create(
  street: 'Street',
  street_number: 100,
  unit: '8A',
  suburb: 'Suburb',
  town: 'Town',
  zip_code: 6012,
  icp: '1234567890ABCDE',
  meter_decimal_places: 4
)

Customer.create(
  first_name: 'Matt',
  last_name: 'Smith',
  email: 'spoof@123.com',
  address_id: 1
)

Admin.create(
  first_name: 'Matt',
  last_name: 'Smith',
  email: 'admin@minishop.com'
)

Reading.create(
  reading: 1.0000,
  customer_id: 1,
  read_by_type: 'Admin',
  read_by_id: 1
)

Price.create(
  price: 0.21
)